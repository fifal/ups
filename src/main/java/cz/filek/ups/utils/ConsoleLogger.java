package cz.filek.ups.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Filip Jani on 19.1.17.
 */
public class ConsoleLogger {
    private static Lock mutex = new ReentrantLock(true);

    private static void log(String message){
        System.out.println(getTime() + message);
    }

    public static void info(String message){
        mutex.lock();
        log("[INFO]" + message);
        mutex.unlock();
    }

    public static void debug(String message){
        mutex.lock();
        log("[DEBUG]" + message);
        mutex.unlock();
    }

    private static String getTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("[HH:mm:ss]");
        return sdf.format(cal.getTime());
    }
}
