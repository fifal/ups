package cz.filek.ups.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;
import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by fifal on 20.10.16.
 * //TODO: někde bug v getByteArrayFromString při čtení
 */
public class ImageUtils {
    /**
     * Returns WritableImage as byte array
     *
     * @param writableImage writable image
     * @return byte array if successful, otherwise returns null
     */
    public static byte[] getByteArrayFromImg(WritableImage writableImage) {
        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(writableImage, null);
        BufferedImage out = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        ColorConvertOp op = new ColorConvertOp(bufferedImage.getColorModel().getColorSpace(), ColorSpace.getInstance(ColorSpace.CS_sRGB),  null);
        op.filter(bufferedImage, out);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] imgArray;
        try {
            ImageIO.write(out, "png", byteArrayOutputStream);
            byteArrayOutputStream.flush();
            imgArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return imgArray;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns byte array as WritableImage
     *
     * @param byteArray byte array
     * @return WritableImage if successful, otherwise returns null
     */
    public static WritableImage getImgFromByteArray(byte[] byteArray) {
        WritableImage writableImage;
        try {
            InputStream inputStream = new ByteArrayInputStream(byteArray);
            BufferedImage bufferedImage = ImageIO.read(inputStream);
            writableImage = SwingFXUtils.toFXImage(bufferedImage, null);
            return writableImage;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Encodes byteArray as a string, so we can send it through text TCP protocol
     *
     * @param imgArray byte array
     * @return String
     */
    public static String getStringFromByteArray(byte[] imgArray) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(imgArray);
            gzipOutputStream.finish();
            gzipOutputStream.flush();
            gzipOutputStream.close();
            byte[] array = byteArrayOutputStream.toByteArray();
            return new sun.misc.BASE64Encoder().encode(array);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Decodes string as a byte array
     *
     * @param imgString
     * @return
     */
    public static byte[] getByteArrayFromString(String imgString) {
        try {
            byte[] imgArray = new sun.misc.BASE64Decoder().decodeBuffer(imgString);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imgArray);
            GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);

            byte[] buffer = new byte[1024];
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int len;
            while ((len = gzipInputStream.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            out.close();
            gzipInputStream.close();
            return out.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
