package cz.filek.ups.network;

/**
 * Created by fifal on 19.10.16.
 */
public class NetworkCodes {
    public static String PROTOCOL_PREFIX = "ups:";
    public static String PROTOCOL_SUFFIX = ";";

    public static String getMessage(NetworkCode networkCode) {
        switch (networkCode) {
            case CL_CONNECT:
                return "cl_connect";
            case CL_DISCONNECT:
                return "cl_disconnect";
            case CL_GUESS:
                return "cl_guess";
            case CL_IMAGE:
                return "cl_image";
            case CL_READY:
                return "cl_ready";
            case SRV_NOT_FOUND:
                return "srv_notFound";
            case SRV_DROPPED:
                return "srv_dropped;";
            case CL_ACK:
                return "cl_ack:";
            default:
                return "";
        }
    }

    public static NetworkCode getNetworkCode(String message) {
        switch (message) {
            case "srv_connected":
                return NetworkCode.SRV_SUCCESSFULLY_LOGGED;
            case "srv_nickExists":
                return NetworkCode.SRV_NAME_TAKEN;
            case "srv_serverFull":
                return NetworkCode.SRV_SERVER_FULL;
            case "srv_notFound":
                return NetworkCode.SRV_NOT_FOUND;
            case "srv_image":
                return NetworkCode.SRV_IMAGE;
            case "srv_draw":
                return NetworkCode.SRV_DRAW;
            case "srv_word":
                return NetworkCode.SRV_WORD;
            case "srv_btnsGuess":
                return NetworkCode.SRV_BTNS_GUESS;
            case "srv_info":
                return NetworkCode.SRV_INFO;
            case "srv_score":
                return NetworkCode.SRV_SCORE;
            case "srv_disconnect":
                return NetworkCode.SRV_DISCONNECT;
            case "srv_dropped":
                return NetworkCode.SRV_DROPPED;
            case "srv_badName":
                return NetworkCode.SRV_BAD_NAME;
            case "srv_ack":
                return NetworkCode.SRV_ACK;
            case "srv_notValidMessage":
                return NetworkCode.SRV_NOT_VALID;
            default:
                return NetworkCode.NOT_NETWORK_CODE;
        }
    }
}
