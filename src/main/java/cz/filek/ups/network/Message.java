package cz.filek.ups.network;

import java.util.Random;

/**
 * @author Filip Jani on 17.1.17.
 */
public class Message {
    private String text;
    private NetworkCode networkCode;
    private long hashCode;
    private boolean isConfirmed;
    private long sentTime;

    public Message(String text, NetworkCode networkCode) {
        this.text = text;
        this.networkCode = networkCode;
        this.hashCode = (text + NetworkCodes.getMessage(networkCode) + new Random().nextDouble()).hashCode();
        this.isConfirmed = false;
        this.sentTime = System.currentTimeMillis();
    }

    public long getHashCode() {
        return hashCode;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void confirm() {
        isConfirmed = true;
    }

    @Override
    public String toString() {
        if (text.equals("")) {
            return NetworkCodes.PROTOCOL_PREFIX + NetworkCodes.getMessage(networkCode) + ":" + hashCode + NetworkCodes.PROTOCOL_SUFFIX;
        } else {
            return NetworkCodes.PROTOCOL_PREFIX + NetworkCodes.getMessage(networkCode) + ":" + text + ":" + hashCode + NetworkCodes.PROTOCOL_SUFFIX;
        }
    }

    public void updateSentTime(){
        this.sentTime = System.currentTimeMillis();
    }

    public long getSentTime(){
        return this.sentTime;
    }
}
